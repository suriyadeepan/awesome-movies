# Movies

## Oh Shit!

- [Fight Club](https://www.rottentomatoes.com/m/fight_club/)
- [V for Vendetta](https://www.rottentomatoes.com/m/v_for_vendetta/)
- [The Matrix](https://www.rottentomatoes.com/m/matrix/)

## Capitalism and The Great American Dream

- [Clerks](https://www.rottentomatoes.com/m/clerks/)
- [Office Space](https://www.rottentomatoes.com/m/office_space/)
- [American Beauty](https://www.rottentomatoes.com/m/american_beauty)

## Distopia/Utopia

- [Equilibrium](https://www.rottentomatoes.com/m/equilibrium/)
- [1984](https://www.rottentomatoes.com/m/1984_1984)
- [12 Monkeys](https://www.rottentomatoes.com/m/12_monkeys/)
- [Dark City](https://www.rottentomatoes.com/m/dark_city/)

## Cerebral

- [Primer](https://www.rottentomatoes.com/m/primer/)
- [2001 : A Space Odyssey](https://www.rottentomatoes.com/m/1000085_2001_a_space_odyssey)
- [Pi](https://www.rottentomatoes.com/m/pi/)
- [La Habitación de Fermat](https://www.rottentomatoes.com/m/fermats_room)

## Mindfuck

- [Another Earth](https://www.rottentomatoes.com/m/another_earth)
- [Coherence](https://www.rottentomatoes.com/m/coherence_2013/)

## Truly Disturbing

- [We need to talk about Kevin](https://www.rottentomatoes.com/m/we_need_to_talk_about_kevin/)
- [Requiem for a Dream](https://www.rottentomatoes.com/m/requiem_for_a_dream)

## Game

- [Cube](https://www.rottentomatoes.com/m/cube)
- [Saw series](https://www.rottentomatoes.com/franchise/saw)

## Dysfunctional Relationship

- [We need to talk about Kevin](https://www.rottentomatoes.com/m/we_need_to_talk_about_kevin/)
- [We are what we are](https://www.rottentomatoes.com/m/we_are_what_we_are_2013)

## We are Drifters/Hobos/Hippies

- [The Master](https://www.rottentomatoes.com/m/the_master_2011)
- [Into the Wild](https://www.rottentomatoes.com/m/into_the_wild/)
- [Inherent Vice](https://www.rottentomatoes.com/m/inherent_vice/)


## Time Travel

- [Predestination](https://www.rottentomatoes.com/m/predestination) (*in-place, deterministic, fixed timeline*)
- [Los Cronocrímenes](https://www.rottentomatoes.com/m/time_crimes)
- [Primer](https://www.rottentomatoes.com/m/primer/)
- [Donny Darko](https://www.rottentomatoes.com/m/donnie_darko)
- [The Butterfly Effect](https://www.rottentomatoes.com/m/butterfly_effect)
- [Edge of Tomorrow](https://www.rottentomatoes.com/m/live_die_repeat_edge_of_tomorrow) (*return-by-death*)
- [Looper](https://www.rottentomatoes.com/m/looper)


## Psychological Thriller

- [The Prestige](https://www.rottentomatoes.com/m/prestige)
- [Oblivion](https://www.rottentomatoes.com/m/oblivion_2013/)
- [Memento](https://www.rottentomatoes.com/m/memento/)


## Thoreauvian

- [Upstream Color](https://www.rottentomatoes.com/m/upstream_color/)
- [Mindwalk](https://www.rottentomatoes.com/m/mindwalk/)
- [Into the Wild](https://www.rottentomatoes.com/m/into_the_wild/)
- [Captain Fantastic](https://www.rottentomatoes.com/m/captain_fantastic/)
- [Dead Poet's Society](https://www.rottentomatoes.com/m/dead_poets_society/)
- [I heart Huckabees](https://www.rottentomatoes.com/m/i_heart_huckabees)

## Save Yourself; Kill them all

- [Silence of the Lambs](https://www.rottentomatoes.com/m/silence_of_the_lambs/)
- [Red Dragon](https://www.rottentomatoes.com/m/red_dragon)
- [Manhunter](https://www.rottentomatoes.com/m/1013248_manhunter)
- [Citizen X](https://www.rottentomatoes.com/m/citizen_x)
- [Fargo](https://www.rottentomatoes.com/m/fargo/)
- [7](https://www.rottentomatoes.com/m/seven)
- [American Psycho](https://www.rottentomatoes.com/m/american_psycho/)
- [Zodiac](https://www.rottentomatoes.com/m/zodiac/)
- [I saw the devil](https://www.rottentomatoes.com/m/akmareul_boattda/)
- [Monster](https://www.rottentomatoes.com/m/1128647_monster)
- [The Chaser](https://www.rottentomatoes.com/m/chaser)
- [Memories of Murder](https://www.rottentomatoes.com/m/memories_of_murder)
- [Perfume : The Story of a Murderer](https://www.rottentomatoes.com/m/perfume_the_story_of_a_murderer/)
- [Identity](https://www.rottentomatoes.com/m/identity/)
- [No Country for Old men](https://www.rottentomatoes.com/m/no_country_for_old_men/)

## Philosophy

- [The Big Lebowski](https://www.rottentomatoes.com/m/big_lebowski)

## Misc

- [Taxi Driver](https://www.rottentomatoes.com/m/taxi_driver)

# TV

*coming soon!*


## Reference

1. [The 15 Best Movies Influenced By Henry David Thoreau](http://www.tasteofcinema.com/2015/the-15-best-movies-influenced-by-henry-david-thoreau-2/)